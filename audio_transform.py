import os
from tqdm import tqdm

from argparse import ArgumentParser

from os.path import isdir, join, exists

from sox.transform import Transformer

from utils import (
    augment_noise,
    augment_volume,
    augment_random_pad,
    augment_random_volume,
    augment_random_speed,
    augment_overlay,
    concat_sounds
)

TRANSFORMER_ARGS = ["contrast", "speed", "dcshift", "pad"]
DEFAULT_ARGS = {
    "contrast": [75],
    "speed": [0.9],
    "dcshift": [-1],
    "pad": [1, 0],  # 1 sec left / 0 sec right
    "noise": [0],  # 0 add noise to the entire / 1 add noise to random intervals
    "volume": [1],  # 0 - decrease volume / 1 - increase volume
    "random_pad": [1],  # add 1sec pad to random interval
    "random_volume": None,
    "random_speed": None,
    'add_background': None
}
FUNCS = {
    "noise": augment_noise,
    "volume": augment_volume,
    "random_pad": augment_random_pad,
    "random_volume": augment_random_volume,
    "random_speed": augment_random_speed,
    "add_background": augment_overlay,
    "concat": concat_sounds
}


class AudioAug:
    def __init__(self, transform, params=None):
        if params is None:
            params = []
        self.transform = transform
        self.params = params
        if not self.params:
            self.params = DEFAULT_ARGS[self.transform]

    def _get_out_path(self, filename, out_dir):
        """
        The _get_out_path function takes in a filename and an output directory.
        It replaces the .wav extension with _transform_name.wav, where transform_name is the name of the transformation being performed on that file.
        The function then returns a string containing the full path to where this new file will be saved.
        
        :param self: Access variables that belongs to the class
        :param filename: Replace the 
        :param out_dir: Specify the directory in which to save the transformed audio file
        :return: The path to the output directory and the name of the file
        """
        filename = filename.replace(".wav", f"_{self.transform}.wav")  # + '_' + str(len(os.listdir(out_dir)))}.wav")
        return join(out_dir, filename)

    def transform_audio(self, in_path, out_path):
        """
        The transform_audio function takes in a path to an audio file and the name of a transform
        as arguments. It then applies that transform to the input audio file, saving it as an output
        audio file with the same name but different extension. The function returns nothing.
        
        :param self: Access the attributes of the class
        :param in_path: Specify the path of the input file
        :param out_path: Specify the output file path
        :return: The array of the transformed audio
        """
        if self.transform in TRANSFORMER_ARGS:
            tfm = Transformer()
            getattr(tfm, self.transform)(*self.params)
            getattr(tfm, "build")(in_path, out_path)
            _ = tfm.build_array(in_path)
        else:
            if self.params:
                FUNCS[self.transform](in_path, out_path, *self.params)
            else:
                FUNCS[self.transform](in_path, out_path)

    def apply(self, input_path, out_dir):
        """
        The apply function takes a path to an audio file and writes the transformed
        audio to a specified output directory. If the input is a directory, it will
        apply the transformation to every file in that directory.
        
        :param self: Access the class attributes
        :param input_path: Specify the path to the audio file that is going to be transformed
        :param out_dir: Specify the output directory
        :return: The output path of the transformed file
        """
        if not exists(out_dir):
            os.mkdir(out_dir)
        if isdir(input_path):
            for filename in tqdm(os.listdir(input_path)):
                in_path = join(input_path, filename)
                out_path = self._get_out_path(filename, out_dir)
                self.transform_audio(in_path, out_path)
        else:
            filename = input_path.split("/")[-1]
            out_path = self._get_out_path(filename, out_dir)
            self.transform_audio(input_path, out_path)


parser = ArgumentParser()
parser.add_argument("--in_path", required=True)
parser.add_argument("--out_dir", required=True)
parser.add_argument("--transform", required=True)
parser.add_argument("--params", nargs="+", default=[], required=False)


if __name__ == "__main__":
    args = parser.parse_args()
    params = []
    for i in args.params:
        try:
            params.append(float(i))
        except ValueError:
            params.append(i)
    augmentor = AudioAug(args.transform, params)

    augmentor.apply(args.in_path, args.out_dir)
