import os
import numpy as np
import librosa
import soundfile as sf
import random
import librosa.display

from os.path import join
from scipy.io import wavfile
from pydub import AudioSegment
from pydub.utils import make_chunks


AUGMENTED_DIR = "aug_audio"
CONVERTED_DIR = "wav_audio_1204"


def augment_noise(inp: str, out: str, rand: bool = False):
    """
    The augment_noise function takes a .wav file and adds noise to it.
    It can also add noise to random slices of the audio file if rand is set to True.
    The function returns a new .wav file with the added noise.
    
    :param inp:str: Specify the path to the 
    :param out:str: Specify the path to which the augmented file is saved
    :param rand:bool=False: Specify whether the noise should be applied to a random slice of the audio file or not
    :return: The path to the augmented file
    """
    signal, sr = librosa.load(inp, sr=None)
        
    noise = np.random.normal(0, np.sqrt(np.mean(signal ** 2)), signal.shape[0])
    if rand:
        num_intervals = np.random.randint(2, 5)
        separators = sorted(
            np.random.randint(0, signal.shape[0] // 4096, num_intervals)
        )
        for i in range(num_intervals - 1):
            noise[separators[i] * 4096:separators[i + 1] * 4096] = 0
    signal_noise = signal + noise * 0.1
    sf.write(out, signal_noise, sr)
    

def augment_random_volume(inp, out):
    """
    The augment_random_volume function takes in a file path and outputs a new file with the volume augmented.
    The function takes in two parameters:
        1) The input file path (inp)
        2) The output file path (out)
    
    :param inp: Specify the input file
    :param out: Specify the output file name
    :return: The augmented signal
    """
    signal, sr = librosa.load(inp)

    num_intervals = np.random.randint(2, 4)
    separators = np.random.randint(0, signal.shape[0] // 4096, num_intervals)
    separators = sorted(list(set(separators)))

    for i in range(len(separators) - 1):
        db = random.choice([0.4, 0.5, 0.6, 1.5, 2])
        signal[separators[i] * 4096:separators[i + 1] * 4096] *= db

    sf.write(out, signal, sr)


def augment_random_speed(inp, out):
    """
    The augment_random_speed function takes in a file path and outputs a new file with the speed of the audio changed.
    The function takes in an input directory and output directory as parameters, which are both strings. The function then 
    opens up the inputted wavfile, reads it into memory, changes its speed by choosing a random number between 0.7 and 1.3 
    as its rate parameter for librosa's time_stretch method (which speeds up or slows down an audio signal), then writes that 
    newly altered signal to the output directory.
    
    :param inp: Specify the input file
    :param out: Specify the output file name
    :return: A new audio file with a random speed between 70% and 130% of the original speed
    """
    signal, sr = librosa.load(inp, sr=None)

    new_signal = np.array([])

    num_intervals = np.random.randint(2, 4)
    separators = np.random.randint(0, signal.shape[0] // 4096, num_intervals)
    separators = sorted(list(set(separators)))

    if 0 not in separators:
        separators.insert(0, 0)
    if signal.shape[0] // 4096 not in separators:
        separators.append(signal.shape[0] // 4096)

    for i in range(len(separators) - 1):
        rate = random.choice([0.7, 1.3])
        new_signal = np.append(
            new_signal,
            librosa.effects.time_stretch(
                signal[separators[i] * 4096:separators[i + 1] * 4096], rate=rate
            ),
        )

    new_signal = np.append(new_signal, signal[separators[-1] * 4096:])

    sf.write(out, new_signal, sr)


def augment_volume(inp, out, increase: bool = True):
    """
    The augment_volume function takes in a wav file and increases or decreases the volume of the audio by a random amount.
    The function takes three arguments:
    inp - The input wav file to be changed.
    out - The output wav file that will be created with the change applied. 
    increase - A boolean value that determines whether or not to increase (True) or decrease (False) volume.
    
    :param inp: Specify the input file
    :param out: Specify the output file name
    :param increase:bool=True: Indicate whether the volume should be increased or decreased
    :return: The input file with the volume increased or decreased by a random amount
    """
    signal = AudioSegment.from_wav(inp)

    db = np.random.randint(5, 30)
    if increase:
        signal += db
    else:
        signal -= db

    signal.export(out, "wav")


def augment_random_pad(inp: str, out: str, pad_size_sec: float = 1.0):
    """
    The augment_random_pad function takes an input file and pads a random section of the signal with zeros.
    The function takes three arguments: inp, out, and pad_size_sec. The inp argument is the path to the input file
    and must be a .wav file. The out argument is where you want to save your output (must end with .wav). 
    The pad size sec argument determines how long each section of padding will be.
    
    :param inp:str: Specify the input file
    :param out:str: Specify the output file name
    :param pad_size_sec:float=1.0: Define the size of the padding to be added at random
    :return: A tuple of the augmented signal and its sampling rate
    """
    signal, sr = librosa.load(inp, sr=None)
    pad = np.zeros(int(sr * pad_size_sec))

    separator = np.random.randint(signal.shape[0] // 3, signal.shape[0])

    sf.write(out, np.insert(signal, separator, pad), sr)


def get_loudness(sound: AudioSegment, slice_size: int=60 * 10000):
    """
    The get_loudness function takes an AudioSegment object as its first parameter.
    It also takes an optional slice_size parameter, which defaults to 60 * 10000.
    The function returns the maximum dBFS value of any 60-second chunk of the audio file.
    
    :param sound:AudioSegment: Specify the sound that we want to get loudness from
    :param slice_size:int=60*10000: Specify the size of each chunk that is processed
    :return: The maximum loudness of a given sound
    """
    return max(chunk.dBFS for chunk in make_chunks(sound, slice_size))


def augment_overlay(inp: str, out: str, back_sound: str, alpha: float = 0.5):
    """
    The augment_overlay function takes in a signal and overlays it with another sound.
    The function takes in the path to the original signal, the path to background sound,
    the output file name, and an optional parameter alpha which is set to 0.5 by default.
    
    :param inp:str: Specify the path to the input file
    :param out:str: Define the path to the output file
    :param back_sound:str: Specify the background sound to be used
    :param alpha:float=0.5: Set the volume of the background sound
    :return: A new wav file with the overlay of the input audio and background sound
    """
    if os.path.isdir('background/' + back_sound):
        path_to_sounds = 'background/' + back_sound
        path_back_sound = os.path.join(path_to_sounds, random.choice(os.listdir(path_to_sounds)))
    else:
        path_back_sound = back_sound

    signal = AudioSegment.from_file(inp)
    back_sound = AudioSegment.from_file(path_back_sound)

    signal_loudness = get_loudness(signal)
    back_sound_loudness = get_loudness(back_sound)

    delta_loudness = signal_loudness - back_sound_loudness

    back_sound += (1 - alpha) * (back_sound_loudness + delta_loudness)

    new_signal = signal.overlay(back_sound, loop=True)
    new_signal = new_signal.set_frame_rate(signal.frame_rate)
    new_signal.export(out, "wav")


def concat_sounds(inp: str, out: str, concat_sound: str, pad_size_sec: float = 0.5):
    """
    The concat_sounds function takes in an input sound file and concatenates another sound file to it.
    The function also takes in the sample rate of the input audio, and pads the end of the output with 0s so that
    the length is consistent. The function returns a new .wav file containing both sounds.
    
    :param inp:str: Specify the input file
    :param out:str: Specify the name of the output file
    :param concat_sound:str: Specify the name of the sound file that will be concatenated to the input
    :param pad_size_sec:float=0.5: Add a certain amount of silence to the end of the signal
    :return: The concatenated sound
    """
    signal, sr = librosa.load(inp, sr=None)
    concat_signal, sr_2 = librosa.load(concat_sound, sr=None)
    pad = np.zeros(int(sr * pad_size_sec))

    new_signal = np.append(signal, np.append(pad, concat_signal))

    sf.write(out, new_signal, sr)


def clear_dir(x):
    """
    The clear_dir function removes all files from a directory.
    
    :param x: Specify the directory that will be cleared of all files
    :return: Nothing
    """
    for file in os.listdir(x):
        os.remove(join(x, file))


def read_wav(x):
    """
    The read_wav function reads in a wav file and returns the data as well as the sampling rate.
    
    :param x: Specify the file path
    :return: A tuple of the sample rate and a numpy array of the data
    """
    return wavfile.read(x)
